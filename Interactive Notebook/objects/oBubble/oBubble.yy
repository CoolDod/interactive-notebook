{
    "id": "b40b0c32-0d9f-45c0-b3b6-62703b664790",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBubble",
    "eventList": [
        {
            "id": "50872787-7c6f-4a2e-973f-af0189a9a440",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b40b0c32-0d9f-45c0-b3b6-62703b664790"
        },
        {
            "id": "e2304ad7-a50a-40ab-a194-f848546bc936",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b40b0c32-0d9f-45c0-b3b6-62703b664790"
        },
        {
            "id": "a0965e24-5498-4d42-9fd6-ed2a7b186da6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b40b0c32-0d9f-45c0-b3b6-62703b664790"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}