/// @desc

#region CHECK CONTROLS
	var lmb = mouse_check_button(mb_left);
	var lmb_pressed = mouse_check_button_pressed(mb_left);
	var lmb_released = mouse_check_button_released(mb_left);
#endregion

#region CHECK HIGHLIGHTED
	var mouseOverBubble = mouse_in_rectangle(x - resizeAreaMargin, y - resizeAreaMargin, x2 + resizeAreaMargin, y2 + resizeAreaMargin)

	if (!selected && mouseOverBubble)
		highlighted = true;
	else
		highlighted = false;
#endregion

#region	CHANGE CURSOR
	if (!(moving || resizing)) {
		if (mouseOverBubble)
			resizeArea = check_areas();
		else
			resizeArea = resizeAreas.none;
	}
#endregion

if (lmb_pressed) {
	#region CHECK SELECT
		if (highlighted) {
			selected = true;
			highlighted = false;
		}
		else if (selected && !mouseOverBubble)
			selected = false;
	#endregion
	
	#region CHECK RESIZE MODE
		if (resizeArea == resizeAreas.move) {
			moving = true;
			oCursor.resetCursor = false;
		
			mouseXOffset = mouse_x - x;
			mouseYOffset = mouse_y - y;
		}
		else if (resizeArea != resizeAreas.none) {
			resizing = true;
			oCursor.resetCursor = false;
		
			if (resizeArea == resizeAreas.west || resizeArea == resizeAreas.northwest || resizeArea == resizeAreas.southwest)
				mouseXOffset = mouse_x - x;
			else if (resizeArea == resizeAreas.east || resizeArea == resizeAreas.northeast || resizeArea == resizeAreas.southeast)
				mouseXOffset = mouse_x - x2;
			
			if (resizeArea == resizeAreas.north || resizeArea == resizeAreas.northwest || resizeArea == resizeAreas.northeast)
				mouseYOffset = mouse_y - y;
			else if (resizeArea == resizeAreas.south || resizeArea == resizeAreas.southwest || resizeArea == resizeAreas.southeast)
				mouseYOffset = mouse_y - y2;
		}
	#endregion
}

#region MOVE BUBBLE
	if (moving) {
		if (lmb_released) {
			oCursor.resetCursor = true;
			
			moving = false;
			calculate_areas();
		}
		
		x = mouse_x - mouseXOffset;
		y = mouse_y - mouseYOffset;
		
		calculate_coordinates();
	}
#endregion

#region RESIZE BUBBLE
	if (resizing) {
		if (lmb_released) {
			oCursor.resetCursor = true;
			
			resizing = false;
			calculate_areas();
		}
		
		if (resizeArea == resizeAreas.west || resizeArea == resizeAreas.northwest || resizeArea == resizeAreas.southwest) {
			x = min(mouse_x - mouseXOffset, x2 - minWidth);
			width = max(x2 - x, minWidth);
		}
		else if (resizeArea == resizeAreas.east || resizeArea == resizeAreas.northeast || resizeArea == resizeAreas.southeast) {
			x2 = mouse_x - mouseXOffset;
			width = max(x2 - x, minWidth);
		}
		
		if (resizeArea == resizeAreas.north || resizeArea == resizeAreas.northwest || resizeArea == resizeAreas.northeast) {
			y = min(mouse_y - mouseYOffset, y2 - minHeight);
			height = max(y2 - y, minHeight);
		}
		else if (resizeArea == resizeAreas.south || resizeArea == resizeAreas.southwest || resizeArea == resizeAreas.southeast) {
			y2 = mouse_y - mouseYOffset;
			height = max(y2 - y, minHeight);
		}
		
		calculate_coordinates();
	}
#endregion
