/// @desc

highlighted = false;
highlightedOutlineColour = c_orange;
highlightedOutlineWidth = 2;

selected = false;
selectedOutlineColour = c_yellow;
selectedOutlineWidth = 2;

text = "WSAD moves the player";
fontSize = 14;
fontScale = fontSize / 12;

marginX = 50;
marginY = 30;

borderWidth = sprite_get_width(sNineSlice) / 3;
borderHeight = sprite_get_height(sNineSlice) / 3;

width = string_width(text) * fontScale + marginX;
height = string_height(text) * fontScale + marginY;

minWidth = borderWidth * 2 + 1;
minHeight = borderHeight * 2 + 1;

x2 = x + width;
y2 = y + height;

cx = x + width / 2;
cy = y + height / 2;

moving = false;
resizing = false;
mouseXOffset = 0;
mouseYOffset = 0;

resizeAreaMargin = 4;

enum resizeAreas {
	none,
	move,
	north,
	east,
	south,
	west,
	northwest,
	northeast,
	southwest,
	southeast
}

resizeArea = resizeAreas.none;

// Calculate move and resize areas
calculate_areas();