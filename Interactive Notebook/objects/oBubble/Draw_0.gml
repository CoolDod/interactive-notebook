/// @desc

if (highlighted) {
	draw_set_colour(highlightedOutlineColour);
	draw_roundrect(x - highlightedOutlineWidth, y - highlightedOutlineWidth, x2 + highlightedOutlineWidth, y2 + highlightedOutlineWidth, true);
	draw_set_colour(-1);
}
else if (selected) {
	draw_set_colour(selectedOutlineColour);
	draw_roundrect(x - selectedOutlineWidth, y - selectedOutlineWidth, x2 + selectedOutlineWidth, y2 + selectedOutlineWidth, true);
	draw_set_colour(-1);
}

draw_nine_slice(sNineSlice, 0, x, y, x2, y2, false);

draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_set_color(c_black)
draw_text_transformed(cx, cy, text, fontScale, fontScale, 0);
draw_set_color(-1);

draw_set_valign(-1);
draw_set_halign(-1);

draw_areas();

draw_text(10, 10, "Coordinates: (" + string(x) + "," + string(y) + ")");
draw_text(10, 30, "Moving: " + string(moving));
draw_text(10, 50, "Resize Mode: " + string(resizeArea));