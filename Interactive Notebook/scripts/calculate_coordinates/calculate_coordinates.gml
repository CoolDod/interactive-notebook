#region SCRIPT DESCRIPTION
	/// @func calculate_coordinates()
	/// @desc Calculates the coordinate of the bubble
#endregion

#region CALCULATE COORDINATES
	x2 = x + width;
	y2 = y + height;
	
	cx = x + width / 2;
	cy = y + height / 2;
#endregion