#region SCRIPT DESCRIPTION
	/// @func mouse_in_rectangle(x1,y1,x2,y2)
	/// @desc Returns true if the mouse is within the given rectangle
	/// @arg {real} x1 The left most coordinate of the rectangle
	/// @arg {real} y1 The top most coordinate of the rectangle
	/// @arg {real} x2 The right most coordinate of the rectangle
	/// @arg {real} y2 The bottom most coordinate of the rectangle
#endregion
#region DECLARE VARIABLES
	var _x1 = argument0;
	var _y1 = argument1;
	var _x2 = argument2;
	var _y2 = argument3;
#endregion


#region MOUSE IN RECTANGLE
	return (point_in_rectangle(mouse_x, mouse_y, _x1, _y1, _x2, _y2));
#endregion