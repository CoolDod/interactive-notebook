#region SCRIPT DESCRIPTION
	/// @func check_areas()
	/// @desc Returns the area the mouse cursor is in
#endregion

#region CHECK AREAS
	// Move bubble
	if (mouse_in_rectangle(moveAreaX1, moveAreaY1, moveAreaX2, moveAreaY2)) {
		window_set_cursor(cr_size_all);
		return resizeAreas.move;
	}
	// Resize bubble west
	else if (mouse_in_rectangle(resizeAreaWX1, resizeAreaWY1, resizeAreaWX2, resizeAreaWY2)) {
		window_set_cursor(cr_size_we);
		return resizeAreas.west;
	}
	// Resize bubble east
	else if (mouse_in_rectangle(resizeAreaEX1, resizeAreaEY1, resizeAreaEX2, resizeAreaEY2)) {
		window_set_cursor(cr_size_we);
		return resizeAreas.east;
	}	
	// Resize bubble north
	else if (mouse_in_rectangle(resizeAreaNX1, resizeAreaNY1, resizeAreaNX2, resizeAreaNY2)) {
		window_set_cursor(cr_size_ns);
		return resizeAreas.north;
	}
	// Resize bubble south
	else if (mouse_in_rectangle(resizeAreaSX1, resizeAreaSY1, resizeAreaSX2, resizeAreaSY2)) {
		window_set_cursor(cr_size_ns);
		return resizeAreas.south;
	}
	// Resize bubble north west
	else if (mouse_in_rectangle(resizeAreaNWX1, resizeAreaNWY1, resizeAreaNWX2, resizeAreaNWY2)) {
		window_set_cursor(cr_size_nwse);
		return resizeAreas.northwest;
	}
	// Resize bubble north east
	else if (mouse_in_rectangle(resizeAreaNEX1, resizeAreaNEY1, resizeAreaNEX2, resizeAreaNEY2)) {
		window_set_cursor(cr_size_nesw);
		return resizeAreas.northeast;
	}
	// Resize bubble south west
	else if (mouse_in_rectangle(resizeAreaSWX1, resizeAreaSWY1, resizeAreaSWX2, resizeAreaSWY2)) {
		window_set_cursor(cr_size_nesw);
		return resizeAreas.southwest;
	}
	// Resize bubble south east
	else if (mouse_in_rectangle(resizeAreaSEX1, resizeAreaSEY1, resizeAreaSEX2, resizeAreaSEY2)) {
		window_set_cursor(cr_size_nwse);
		return resizeAreas.southeast;
	}
	// No area
	else
		return resizeAreas.none;
#endregion