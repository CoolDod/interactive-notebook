#region SCRIPT DESCRIPTION
	/// @func draw_nine_slice(sprite,subimg,x1,y1,x2,y2,tiled)
	/// @desc Draws a given sprite using the nine slice method
	/// @arg {int} sprite The index of the sprite to use
	/// @arg {int} subimg The frame of the sprite to use
	/// @arg {int} x1
	/// @arg {int} y1
	/// @arg {int} x2
	/// @arg {int} y2
	/// @arg {bool} tiled Whether or not to tile the sprite
#endregion
#region DECLARE VARIABLES
	var _sprite = argument0;
	var _subimg = argument1;
	var _x1 = argument2;
	var _y1 = argument3;
	var _x2 = argument4;
	var _y2 = argument5;
	var _tiled = argument6;
#endregion


#region DRAW NINE SLICE
	// Get size of one block
	var _size = sprite_get_width(_sprite) / 3;
	// Get nine slice width
	var _width = _x2 - _x1;
	// Get nine slice height
	var _height = _y2 - _y1;
	
	if (_tiled) {
		// Calculate bottom right coordinates
		var _xmod = _x2 - _width % _size;
		var _ymod = _y2 - _height % _size;
		// Get number of tiles
		var _xtiles = (_width - _size) div _size;
		var _ytiles = (_height - _size) div _size;
		
		// CORNERS
		// Draw top left corner
		draw_sprite_part(_sprite, _subimg, 0, 0, _size, _size, _x1, _y1);
		// Draw top right corner
		draw_sprite_part(_sprite, _subimg, _size * 2, 0, _size, _size, _xmod, _y1);
		// Draw bottom left corner
		draw_sprite_part(_sprite, _subimg, 0, _size * 2, _size, _size, _x1, _ymod);
		// Draw bottom right corner
		draw_sprite_part(_sprite, _subimg, _size * 2, _size * 2, _size, _size, _xmod, _ymod);
		
		// SIDES
		// Draw top and bottom sides
		var _i = 0;
		repeat (_xtiles) {
			var _sidex = _x1 + _size * (_i + 1);
			
			draw_sprite_part(_sprite, _subimg, _size, 0, _size, _size, _sidex, _y1);
			draw_sprite_part(_sprite, _subimg, _size, _size * 2, _size, _size, _sidex, _ymod);
			
			_i++;
		}
		// Draw left and right sides
		_i = 0;
		repeat (_ytiles) {
			var _sidey = _y1 + _size * (_i + 1);
			
			draw_sprite_part(_sprite, _subimg, 0, _size, _size, _size, _x1, _sidey);
			draw_sprite_part(_sprite, _subimg, _size * 2, _size, _size, _size, _xmod, _sidey);
			
			_i++;
		}
		
		// CENTRE
		_i = 0;
		repeat (_xtiles) {
			var _j = 0;
			repeat (_ytiles) {
				draw_sprite_part(_sprite, _subimg, _size, _size, _size, _size, _x1 + _size * (_i + 1), _y1 + _size * (_j + 1));
				
				_j++;
			}
			
			_i++;
		}
	}
	else {
		// CORNERS
		// Draw top left corner
		draw_sprite_part(_sprite, _subimg, 0, 0, _size, _size, _x1, _y1);
		// Draw top right corner
		draw_sprite_part(_sprite, _subimg, _size * 2, 0, _size, _size, _x2 - _size, _y1);
		// Draw bottom left corner
		draw_sprite_part(_sprite, _subimg, 0, _size * 2, _size, _size, _x1, _y2 - _size);
		// Draw bottom right corner
		draw_sprite_part(_sprite, _subimg, _size * 2, _size * 2, _size, _size, _x2 - _size, _y2  - _size);
	
		// SIDES
		// Draw top side
		draw_sprite_part_ext(_sprite, _subimg, _size, 0, 1, _size, _x1 + _size, _y1, _width - _size * 2, 1, c_white, 1);
		// Draw bottom side
		draw_sprite_part_ext(_sprite, _subimg, _size, _size * 2, 1, _size, _x1 + _size, _y2 - _size, _width - _size * 2, 1, c_white, 1);
		// Draw left side
		draw_sprite_part_ext(_sprite, _subimg, 0, _size, _size, 1, _x1, _y1 + _size, 1, _height - _size * 2, c_white, 1);
		// Draw right side
		draw_sprite_part_ext(_sprite, _subimg, _size * 2, _size, _size, 1, _x2 - _size, _y1 + _size, 1, _height - _size * 2, c_white, 1);
	
		// CENTRE
		draw_sprite_part_ext(_sprite, _subimg, _size, _size, 1, 1, _x1 + _size, _y1 + _size, _width - _size * 2, _height - _size * 2, c_white, 1);
	}
#endregion