#region SCRIPT DESCRIPTION
	/// @func draw_areas()
	/// @desc Draws all the move and resize areas
#endregion

#region DRAW AREAS
	// Draw settings
	draw_set_colour(c_red);
	draw_set_alpha(0.5);

	// Draw move area
	draw_rectangle(moveAreaX1, moveAreaY1, moveAreaX2, moveAreaY2, false);
	draw_rectangle(moveAreaX1, moveAreaY1, moveAreaX2, moveAreaY2, true);
	
	// Draw resize west area
	draw_rectangle(resizeAreaWX1, resizeAreaWY1, resizeAreaWX2, resizeAreaWY2, false);
	draw_rectangle(resizeAreaWX1, resizeAreaWY1, resizeAreaWX2, resizeAreaWY2, true);
	
	// Draw resize east area
	draw_rectangle(resizeAreaEX1, resizeAreaEY1, resizeAreaEX2, resizeAreaEY2, false);
	draw_rectangle(resizeAreaEX1, resizeAreaEY1, resizeAreaEX2, resizeAreaEY2, true);
	
	// Draw resize north area
	draw_rectangle(resizeAreaNX1, resizeAreaNY1, resizeAreaNX2, resizeAreaNY2, false);
	draw_rectangle(resizeAreaNX1, resizeAreaNY1, resizeAreaNX2, resizeAreaNY2, true);
	
	// Draw resize south area
	draw_rectangle(resizeAreaSX1, resizeAreaSY1, resizeAreaSX2, resizeAreaSY2, false);
	draw_rectangle(resizeAreaSX1, resizeAreaSY1, resizeAreaSX2, resizeAreaSY2, true);
	
	// Draw resize north west area
	draw_rectangle(resizeAreaNWX1, resizeAreaNWY1, resizeAreaNWX2, resizeAreaNWY2, false);
	draw_rectangle(resizeAreaNWX1, resizeAreaNWY1, resizeAreaNWX2, resizeAreaNWY2, true);
	
	// Draw resize north east area
	draw_rectangle(resizeAreaNEX1, resizeAreaNEY1, resizeAreaNEX2, resizeAreaNEY2, false);
	draw_rectangle(resizeAreaNEX1, resizeAreaNEY1, resizeAreaNEX2, resizeAreaNEY2, true);
	
	// Draw resize south west area
	draw_rectangle(resizeAreaSWX1, resizeAreaSWY1, resizeAreaSWX2, resizeAreaSWY2, false);
	draw_rectangle(resizeAreaSWX1, resizeAreaSWY1, resizeAreaSWX2, resizeAreaSWY2, true);
	
	// Draw resize south east area
	draw_rectangle(resizeAreaSEX1, resizeAreaSEY1, resizeAreaSEX2, resizeAreaSEY2, false);
	draw_rectangle(resizeAreaSEX1, resizeAreaSEY1, resizeAreaSEX2, resizeAreaSEY2, true);
	
	// Reset draw settings
	draw_set_alpha(1);
	draw_set_colour(-1);
#endregion