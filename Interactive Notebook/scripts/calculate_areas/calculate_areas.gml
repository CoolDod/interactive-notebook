#region SCRIPT DESCRIPTION
	/// @func calculate_areas()
	/// @desc Calculates the move and resize areas
#endregion


#region CALCULATE AREAS
	// Calculate move area
	moveAreaX1 = x + borderWidth + resizeAreaMargin;
	moveAreaY1 = y + borderHeight + resizeAreaMargin;
	moveAreaX2 = x2 - borderWidth - resizeAreaMargin;
	moveAreaY2 = y2 - borderHeight - resizeAreaMargin;

	// Calculate resize west area
	resizeAreaWX1 = x - resizeAreaMargin;
	resizeAreaWY1 = moveAreaY1;
	resizeAreaWX2 = moveAreaX1;
	resizeAreaWY2 = moveAreaY2;

	// Calculate resize east area
	resizeAreaEX1 = moveAreaX2;
	resizeAreaEY1 = moveAreaY1;
	resizeAreaEX2 = x2 + resizeAreaMargin;
	resizeAreaEY2 = moveAreaY2;
	
	// Calculate resize north area
	resizeAreaNX1 = moveAreaX1;
	resizeAreaNY1 = y - resizeAreaMargin;
	resizeAreaNX2 = moveAreaX2;
	resizeAreaNY2 = moveAreaY1;
	
	// Calculate resize south area
	resizeAreaSX1 = moveAreaX1;
	resizeAreaSY1 = moveAreaY2;
	resizeAreaSX2 = moveAreaX2;
	resizeAreaSY2 = y2 + resizeAreaMargin;
	
	// Calculate resize north west area
	resizeAreaNWX1 = resizeAreaWX1;
	resizeAreaNWY1 = resizeAreaNY1;
	resizeAreaNWX2 = moveAreaX1;
	resizeAreaNWY2 = moveAreaY1;
	
	// Calculate resize north east area
	resizeAreaNEX1 = moveAreaX2;
	resizeAreaNEY1 = resizeAreaNY1;
	resizeAreaNEX2 = resizeAreaEX2;
	resizeAreaNEY2 = moveAreaY1;
	
	// Calculate resize south west area
	resizeAreaSWX1 = resizeAreaWX1;
	resizeAreaSWY1 = moveAreaY2;
	resizeAreaSWX2 = moveAreaX1;
	resizeAreaSWY2 = resizeAreaSY2;
	
	// Calculate resize south east area
	resizeAreaSEX1 = moveAreaX2;
	resizeAreaSEY1 = moveAreaY2;
	resizeAreaSEX2 = resizeAreaEX2;
	resizeAreaSEY2 = resizeAreaSY2;
#endregion