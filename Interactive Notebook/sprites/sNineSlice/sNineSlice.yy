{
    "id": "40990dd1-1864-407d-b753-9abeb485aca7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNineSlice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6deb9a9-3684-486b-b4b3-0bce763ebd0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40990dd1-1864-407d-b753-9abeb485aca7",
            "compositeImage": {
                "id": "f10e9274-80ac-4b2f-8965-c704882d6a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6deb9a9-3684-486b-b4b3-0bce763ebd0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "232df487-02df-4c8f-af56-1ef9656d1fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6deb9a9-3684-486b-b4b3-0bce763ebd0d",
                    "LayerId": "37229a25-bab9-4d46-989a-f8559f6bdc5d"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 15,
    "layers": [
        {
            "id": "37229a25-bab9-4d46-989a-f8559f6bdc5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40990dd1-1864-407d-b753-9abeb485aca7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}